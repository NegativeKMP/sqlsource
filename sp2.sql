SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Калиниченко
-- Create date: 20190423
-- Description:	Рассылка смс сообщений потребителям 
-- =============================================
ALTER PROCEDURE [dbo].[spCbuueSentSMS_onLine04]
      @Login_Cbuue nvarchar(50),
	  @NPredSeti nvarchar(50),
      @NRes nvarchar(50),
	  @NPodstan nvarchar(50),
	  @NLinePS  nvarchar(50),
	  @NTP nvarchar(50),
	  @NTTTP nvarchar(50),
	  @NLineTP04 nvarchar(50),
	  @TEXT nvarchar(1000)/*TODO в будущем заменить на id текста и можно будет убрать костыль*/
AS
BEGIN
    /*
	DECLARE
	@Login_Cbuue nvarchar(50) = 'r',
    @NRes nvarchar(50)  = '02',
	@NPodstan nvarchar(50) = '34',
	@NLinePS nvarchar(50) = '0001',
	@NTP nvarchar(50) =  '0099',
	@NTTTP nvarchar(50) = '2'
	@TEXT nvarchar(MAX) = 'Тест из процедуры рассылки'
    --end try  Begin Catch End Catch
	*/	
	SET NOCOUNT ON;
	DECLARE @id_user int = 1; --заглушка 
	DECLARE @idSmsText int = 0;
	DECLARE @idError int = 0; --0-127 ограничение SQL RAISEERROR for user call
	DECLARE @dateExecuteSp datetime = GetDate();  -- заглушка дата отправки равна дате вызова процедуры
	DECLARE @Res nvarchar(50);
	DECLARE @Podstan nvarchar(100);
	DECLARE @TP nvarchar (100);
	DECLARE @phoneS nvarchar(4000) = '';
	DECLARE @Error nvarchar(1000) = '';
	DECLARE @GUID uniqueidentifier = newid();
    /*
	ErrorNumber	ErrorName
	100		Переданы не все параметры
	101		Смс добавлено для рассылки
	102     Нет номеров телефонов по выбранным условиям
	  0		Не обработанная ошибка	
    */
	Begin Try
	   if    (@Login_Cbuue = '') OR (@Login_Cbuue  is null) 
	      OR (@NPredSeti = '') OR (@NPredSeti  is null)
	      OR (@NRes = '') OR (@NRes  is null)
		  OR (@NPodstan = '') OR (@NPodstan  is null)
		  OR (@NTP = '') OR (@NTP IS NULL)
		  OR (@NTTTP = '') OR (@NTTTP IS NULL)
	      OR (@NLineTP04 = '') OR (@NLineTP04 IS NULL)
		  OR (@NLinePS = '') OR (@NLinePS  is null)
		  OR (@TEXT = '') OR (@TEXT  is null)
		 begin
          set @idError = 100;
		  set @Error = 'Переданы не все параметры';
		  RAISERROR(@Error,16,@idError);
         end;

	   --------------------------------------------------------------
	   set @Res = (select top(1) NameRES from  dbo.TMP_RES where KeyRES = @NRes);
	   set @Podstan = (select top(1) NamePodstan from  dbo.TMP_Podstan where  KeyPodstan= @NPodstan and KeyRES = @NRes);
	   set @Tp = (select top(1) SeatTP from  dbo.BalansTP where NTP = @NTP and KeyRes = @NRes );
	   ---------------------------------------------------------------
       ------------------------------------------------------------
       --set  @TEXT = @TEXT +' :: РЭС '+ @Res + ', подстанция ' + @Podstan 
						declare @tbId_text table (id int) 
						insert into [dbo].[sms_tbSmsText] ([text])
						output inserted.[id_text] [int] into @tbId_text
						values (ltrim(rtrim(@TEXT)))
  	   set @idSmsText = (select top(1) id from @tbId_text)
	   --------------------------------------------------------------
	   --------------------------------------------------------------
	   DECLARE thisCursor CURSOR LOCAL
	   FOR 
	    select distinct '7'+Right(ltrim(rtrim(Mobilephone)),10)  as Mobilephone
		from dbo.Codings as C
		inner join dbo.Potrebiteli as P	on C.KeySbyt  = P.KeySbyt
									and C.KeyOtdSbyt  = P.KeyOtdSbyt
									and C.KeyRegion   = P.KeyRegion
									and C.NLSchet     = P.NLSchet
									and C.KeyContract = P.KeyContract  
									and P.Mobilephone not like '%000000000%'
									and P.Mobilephone like '%[9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'
									and P.Mobilephone not like '%[9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][ ]' 
									and C.State <> '08'
									and C.NLinePS != '0000'
									and C.NTP != '0000' 
									--and Substring(C.NSekcClassNap, 1, 1) = '1'
			where 
				  C.NPredSeti= @NPredSeti and
				  C.NRes     = @NRes and 
				  C.NPodstan = @NPodstan and 
				  C.NLinePS  = @NLinePS	and
				  C.NTP      = @NTP and
				  C.NTTTP    = @NTTTP and
				  C.NLineTP  = @NLineTP04
				  	
	   OPEN thisCursor   
			if @@CURSOR_ROWS = 0
			begin
			  set @idError = 102;
			  set @Error = 'Нет номеров телефонов по выбранным условиям';
			  RAISERROR(@Error,16,@idError);
			end;
		
			DECLARE @Mobilephone nvarchar(11) 
		      
			FETCH NEXT FROM thiscursor INTO @Mobilephone

			WHILE @@FETCH_STATUS = 0  
			BEGIN  
				--------------------------------------------------------
				/*Вставка мобильных номеров в табилцу опроса рассылки */
				--------------------------------------------------------
				   
				insert into [dbo].[sms_tbSMS] ([id_text],[sendtime],[sendersms],[num_telephone],[GUID])
				values (@idSmsText,@dateExecuteSp,@id_user,@Mobilephone,@GUID)  
                     
				---------------------------------------------------------
				set @phoneS = @phoneS+@Mobilephone+';'	            
				FETCH NEXT FROM thiscursor
				INTO  @Mobilephone
			END  
		CLOSE thiscursor;  
		DEALLOCATE thiscursor;
	
		set @idError = 101;
		set @Error = 'Смс добавлено для рассылки';
		INSERT INTO [dbo].[sms_tbJurnal] ([idUser]
												,[idSmsText]
												,[idError]
												,[Login_Cbuue]
												,[DateExecute]
												,[NPredSeti]
												,[Nres]
												,[Res]
												,[NPodstan]
												,[Podstan]
												,[NlinePS]
												,[NTp]
												,[Tp]
												,[NTTTP]
												,[NLineTP04]
												,[Text]
												,[phoneS]
												,[Error]
												,[GUID])
		VALUES (@id_user,@idSmsText,@idError,@Login_Cbuue,@dateExecuteSp,@NPredSeti,@NRes,@Res,@NPodstan,@Podstan,@NLinePS,@NTP,@TP,@NTTTP,@NLineTP04,@TEXT,@phoneS,@Error,@GUID);
		RETURN @idError 

	End Try
    Begin Catch
	   if (Select ERROR_STATE()) not in (100,102)
		begin
			set @idError = (Select ERROR_STATE())
			set @Error = ('ErrorState: '+Convert(nvarchar(50), ERROR_STATE())+'; Line: '+ Convert(nvarchar(100),ERROR_LINE()) +  '; '+ERROR_MESSAGE()); 
		end
       if @Login_Cbuue  is null set @Login_Cbuue = 'call with null';
	   if @NPredSeti  is null   set @NPredSeti   = 'call with null';
	   if @NRes  is null        set @NRes        = 'call with null';
	   if @NPodstan  is null    set @NPodstan    = 'call with null';
	   if @NLinePS is null      set @NLinePS     = 'call with null';
	   if @TEXT  is null        set @TEXT        = 'call with null';
	   if @NTP  is null         set @NTP         = 'call with null';
	   if @NTTTP  is null       set @NTTTP       = 'call with null';
	   if @NLineTP04  is null   set @NLineTP04   = 'call with null';
	   if @Login_Cbuue  = ''    set @Login_Cbuue = 'call with empty string value';
	   if @NPredSeti  = ''      set @NPredSeti   = 'call with empty string value'; 
	   if @NRes  = ''  			set @NRes        = 'call with empty string value';
	   if @NPodstan  = ''  		set @NPodstan    = 'call with empty string value';
	   if @NLinePS = ''         set @NLinePS     = 'call with empty string value';
	   if @TEXT  = ''  			set @TEXT        = 'call with empty string value';
	   if @NTP  = ''  			set @NTP         = 'call with empty string value';
	   if @NLineTP04  = ''  	set @NLineTP04   = 'call with empty string value';
	   

	   INSERT INTO [dbo].[sms_tbJurnal] ([idUser]
												,[idSmsText]
												,[idError]
												,[Login_Cbuue]
												,[DateExecute]
												,[NPredSeti]
												,[Nres]
												,[Res]
												,[NPodstan]
												,[Podstan]
												,[NlinePS]
												,[NTp]
												,[Tp]
												,[NTTTP]
												,[NLineTP04]
												,[Text]
												,[phoneS]
												,[Error]
												,[GUID])
		VALUES (@id_user,@idSmsText,@idError,@Login_Cbuue,@dateExecuteSp,@NPredSeti,@NRes,@Res,@NPodstan,@Podstan,@NLinePS,@NTP,@TP,@NTTTP,@NLineTP04,@TEXT,@phoneS,@Error,@GUID);
		RETURN @idError 
		/*SELECT  
			 ERROR_NUMBER() AS ErrorNumber  
			,ERROR_SEVERITY() AS ErrorSeverity  
			,ERROR_STATE() AS ErrorState  
			,ERROR_PROCEDURE() AS ErrorProcedure  
			,ERROR_MESSAGE() AS ErrorMessage;  */
	End Catch	  
END

