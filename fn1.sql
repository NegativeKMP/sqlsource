SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		KalinichenkoMP
-- Create date: 20170608 
-- Description:	Функция принимает таблицу и номер фильтра, далее передает по номеру в требуемый фильтр и возвращает отфильтрованный набор
-- INPUT: @Points пользовательский табличный тип; @NFilter;
-- =============================================
ALTER FUNCTION [web].[fnP_FilterPoint]	
(
	 @Nfilter int=0,
	 @Points as [web].[tpPoint] READONLY
)
RETURNS 
@tbPoint TABLE
(   [id] [int]  NULL,
	[Time] [datetime] NULL,
	[Lat] [float] NULL,
	[Long] [float] NULL,
	[Altitude] [float] NULL,
	[Accuracy] [int] NULL,
	[Speed] [float] NULL,
	[Bearing] [float] NULL,
	[Q] [float] NULL, 
	[QsatW] [float] NULL,
	[Satellites] [int] NULL,
	[L1] [float] NULL,
	[L2] [float] NULL,
	[L3] [float] NULL,
	[L4] [float] NULL,
	[L5] [float] NULL,
	[L6] [float] NULL,
	[L7] [float] NULL,
	[L8] [float] NULL,
	[L9] [float] NULL,
	[L10] [float] NULL,
	[L11] [float] NULL,
	[L12] [float] NULL,
	[L13] [float] NULL,
	[L14] [float] NULL,
	[L15] [float] NULL,
	[L16] [float] NULL,
	[L17] [float] NULL,
	[L18] [float] NULL,
	[L19] [float] NULL,
	[L20] [float] NULL)
AS
BEGIN
    DECLARE @TMP1 as web.tpPoint
	DECLARE @TMP2 as web.tpPoint


          IF @Nfilter=0 insert @tbPoint select [id],[Time],[Lat],[Long],[Altitude],[Accuracy],[Speed],[Bearing],[Q],[QsatW],[Satellites],[L1],[L2],[L3],[L4],[L5],[L6],[L7],[L8],[L9],[L10],[L11],[L12],[L13],[L14],[L15],[L16],[L17],[L18],[L19],[L20]	from [web].[fnP_Filter_0](@Points) 
	 ELSE IF @Nfilter=1 insert @tbPoint select [id],[Time],[Lat],[Long],[Altitude],[Accuracy],[Speed],[Bearing],[Q],[QsatW],[Satellites],[L1],[L2],[L3],[L4],[L5],[L6],[L7],[L8],[L9],[L10],[L11],[L12],[L13],[L14],[L15],[L16],[L17],[L18],[L19],[L20] from [web].[fnP_Filter_1](@Points)
	 ELSE IF @Nfilter=2 insert @tbPoint select [id],[Time],[Lat],[Long],[Altitude],[Accuracy],[Speed],[Bearing],[Q],[QsatW],[Satellites],[L1],[L2],[L3],[L4],[L5],[L6],[L7],[L8],[L9],[L10],[L11],[L12],[L13],[L14],[L15],[L16],[L17],[L18],[L19],[L20] from [web].[fnP_Filter_2](@Points) 
	 ELSE IF @Nfilter=3 insert @tbPoint select [id],[Time],[Lat],[Long],[Altitude],[Accuracy],[Speed],[Bearing],[Q],[QsatW],[Satellites],[L1],[L2],[L3],[L4],[L5],[L6],[L7],[L8],[L9],[L10],[L11],[L12],[L13],[L14],[L15],[L16],[L17],[L18],[L19],[L20] from [web].[fnP_Filter_3](@Points) 
	 ELSE IF @Nfilter=4 insert @tbPoint select [id],[Time],[Lat],[Long],[Altitude],[Accuracy],[Speed],[Bearing],[Q],[QsatW],[Satellites],[L1],[L2],[L3],[L4],[L5],[L6],[L7],[L8],[L9],[L10],[L11],[L12],[L13],[L14],[L15],[L16],[L17],[L18],[L19],[L20] from [web].[fnP_Filter_4](@Points) 
	 ELSE IF @Nfilter=5 insert @tbPoint select [id],[Time],[Lat],[Long],[Altitude],[Accuracy],[Speed],[Bearing],[Q],[QsatW],[Satellites],[L1],[L2],[L3],[L4],[L5],[L6],[L7],[L8],[L9],[L10],[L11],[L12],[L13],[L14],[L15],[L16],[L17],[L18],[L19],[L20] from [web].[fnP_Filter_5](@Points) 
	 ELSE IF @Nfilter=6 insert @tbPoint select [id],[Time],[Lat],[Long],[Altitude],[Accuracy],[Speed],[Bearing],[Q],[QsatW],[Satellites],[L1],[L2],[L3],[L4],[L5],[L6],[L7],[L8],[L9],[L10],[L11],[L12],[L13],[L14],[L15],[L16],[L17],[L18],[L19],[L20] from [web].[fnP_Filter_6](@Points) 
	 ELSE IF @Nfilter=7 insert @tbPoint select [id],[Time],[Lat],[Long],[Altitude],[Accuracy],[Speed],[Bearing],[Q],[QsatW],[Satellites],[L1],[L2],[L3],[L4],[L5],[L6],[L7],[L8],[L9],[L10],[L11],[L12],[L13],[L14],[L15],[L16],[L17],[L18],[L19],[L20] from [web].[fnP_Filter_7](@Points)  
	 ELSE IF @Nfilter=8 insert @tbPoint select [id],[Time],[Lat],[Long],[Altitude],[Accuracy],[Speed],[Bearing],[Q],[QsatW],[Satellites],[L1],[L2],[L3],[L4],[L5],[L6],[L7],[L8],[L9],[L10],[L11],[L12],[L13],[L14],[L15],[L16],[L17],[L18],[L19],[L20] from [web].[fnP_Filter_8](@Points)
	 ELSE IF @Nfilter=9 insert @tbPoint select [id],[Time],[Lat],[Long],[Altitude],[Accuracy],[Speed],[Bearing],[Q],[QsatW],[Satellites],[L1],[L2],[L3],[L4],[L5],[L6],[L7],[L8],[L9],[L10],[L11],[L12],[L13],[L14],[L15],[L16],[L17],[L18],[L19],[L20] from [web].[fnP_Filter_9](@Points) 	  	 
	 ELSE IF @Nfilter=10 BEGIN 
	                        insert @TMP1 select [id],[Time],[Lat],[Long],[Altitude],[Accuracy],[Speed],[Bearing],[Q],[QsatW],[Satellites],[L1],[L2],[L3],[L4],[L5],[L6],[L7],[L8],[L9],[L10],[L11],[L12],[L13],[L14],[L15],[L16],[L17],[L18],[L19],[L20] from [web].[fnP_Filter_4](@Points) 
							insert @TMP2 select [id],[Time],[Lat],[Long],[Altitude],[Accuracy],[Speed],[Bearing],[Q],[QsatW],[Satellites],[L1],[L2],[L3],[L4],[L5],[L6],[L7],[L8],[L9],[L10],[L11],[L12],[L13],[L14],[L15],[L16],[L17],[L18],[L19],[L20] from [web].[fnP_Filter_5](@tmp1) 
                            insert @tbPoint select [id],[Time],[Lat],[Long],[Altitude],[Accuracy],[Speed],[Bearing],[Q],[QsatW],[Satellites],[L1],[L2],[L3],[L4],[L5],[L6],[L7],[L8],[L9],[L10],[L11],[L12],[L13],[L14],[L15],[L16],[L17],[L18],[L19],[L20] from [web].[fnP_Filter_7](@tmp2) 
	                     END 
    ELSE IF @Nfilter=11 BEGIN 
	                        insert @TMP1 select [id],[Time],[Lat],[Long],[Altitude],[Accuracy],[Speed],[Bearing],[Q],[QsatW],[Satellites],[L1],[L2],[L3],[L4],[L5],[L6],[L7],[L8],[L9],[L10],[L11],[L12],[L13],[L14],[L15],[L16],[L17],[L18],[L19],[L20] from [web].[fnP_Filter_4](@Points) 
							insert @TMP2 select [id],[Time],[Lat],[Long],[Altitude],[Accuracy],[Speed],[Bearing],[Q],[QsatW],[Satellites],[L1],[L2],[L3],[L4],[L5],[L6],[L7],[L8],[L9],[L10],[L11],[L12],[L13],[L14],[L15],[L16],[L17],[L18],[L19],[L20] from [web].[fnP_Filter_5](@tmp1) 
                            insert @tbPoint select [id],[Time],[Lat],[Long],[Altitude],[Accuracy],[Speed],[Bearing],[Q],[QsatW],[Satellites],[L1],[L2],[L3],[L4],[L5],[L6],[L7],[L8],[L9],[L10],[L11],[L12],[L13],[L14],[L15],[L16],[L17],[L18],[L19],[L20] from [web].[fnP_Filter_8](@tmp2) 
	                     END 
	 ELSE insert @tbPoint select [id],[Time],[Lat],[Long],[Altitude],[Accuracy],[Speed],[Bearing],[Q],[QsatW],[Satellites],[L1],[L2],[L3],[L4],[L5],[L6],[L7],[L8],[L9],[L10],[L11],[L12],[L13],[L14],[L15],[L16],[L17],[L18],[L19],[L20] from [web].[fnP_Filter_0](@Points) 
RETURN	
END




