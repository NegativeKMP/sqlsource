SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- опоздания  v2 by КМП  20171124
-- =============================================
ALTER PROCEDURE [dbo].[Opozd]
	@Datetime1 datetime,
	@Datetime2 datetime 
AS
BEGIN
 
/*DECLARE @Result AS TABLE(
	[FIO] [varchar](50) NULL,
	[TabNumber] [int] NULL,
	[Company] [varchar](50) NULL,
	[Otdel] [varchar](50) NULL,
	[Doljnost] [varchar](50) NULL,
	[Date] [date] NULL,
	[Time] [time](7) NULL,
	[Remark] [varchar](255) NULL,
	[mode] [varchar](10) NULL,
	[eTime] [varchar](50) NULL
)*/

SET NOCOUNT ON
SET LANGUAGE US_English
SET DATEFIRST 1 --понедельник первый день недели
 --  declare @Datetime1 as datetime='2017-11-11 00:00:00.000'
 --  declare @Datetime2 as datetime='2017-11-11 23:59:59.000'
  -- declare @Datetime1 as datetime='2016-08-01 00:00:00.000'
   --declare @Datetime2 as datetime='2016-08-31 23:59:59.000'
IF OBJECT_ID(N'tempdb..#plogdata8AM', 'U') IS NOT NULL
DROP TABLE #plogdata8AM
SELECT * INTO #plogdata8AM FROM
( 
	SELECT 
		pList.Name + ' ' + SUBSTRING(pList.FirstName,1,1) + '. '+ SUBSTRING( pList.MidName,1,1) + '.' as FIO, --[Фамилия И. О.]
		pList.TabNumber,
		PCompany.Name as Company, 
		PDivision.Name as Otdel,
		PPost.Name as Doljnost,
		vwPlogdataFix.TimeVal,
		vwPlogdataFix.Remark,
		vwPlogdataFix.Mode,
		vwplogdataFix.HozOrgan	
	FROM           PList
		 left join PDivision     on Plist.Section = PDivision.ID 
		 left join PPost         on Plist.Post    = PPost.ID 
		 left join PCompany      on PList.Company = PCompany.ID
		 left join vwPlogdataFix on PList.ID      = vwPlogdataFix.HozOrgan
    WHERE  vwPlogdataFix.TimeVal BETWEEN @Datetime1 AND @Datetime2
	AND  DATEPART(WEEKDAY,vwPlogdataFix.TimeVal) not in (6,7)
    AND  CONVERT(time, vwPlogdataFix.TimeVal) BETWEEN CONVERT(time, '5:00:00') AND CONVERT(time, '8:30:00')
	--AND (Company in ('11','12','14'))
	AND vwPlogdataFix.Event=28 --32=проход зафиксирован 
	AND vwPlogdataFix.MODE=1 --1=вход							    
) As t8AM

IF OBJECT_ID(N'tempdb..#plogdata12AM', 'U') IS NOT NULL
DROP TABLE #plogdata12AM
SELECT * INTO #plogdata12AM FROM
( 
	SELECT 
		pList.Name + ' ' + SUBSTRING(pList.FirstName,1,1) + '. '+ SUBSTRING( pList.MidName,1,1) + '.' as FIO, --[Фамилия И. О.]
		pList.TabNumber,
		PCompany.Name as Company, 
		PDivision.Name as Otdel,
		PPost.Name as Doljnost,
		vwPlogdataFix.TimeVal,
		vwPlogdataFix.Remark,
		vwPlogdataFix.Mode,
		vwplogdataFix.HozOrgan	
	FROM           PList
		 left join PDivision     on Plist.Section = PDivision.ID 
		 left join PPost         on Plist.Post    = PPost.ID 
		 left join PCompany      on PList.Company = PCompany.ID
		 left join vwPlogdataFix on PList.ID      = vwPlogdataFix.HozOrgan
    WHERE  vwPlogdataFix.TimeVal BETWEEN @Datetime1 AND @Datetime2
	AND  DATEPART(WEEKDAY,vwPlogdataFix.TimeVal) not in (6,7)
    AND  CONVERT(time, vwPlogdataFix.TimeVal) BETWEEN CONVERT(time, '11:15:00') AND CONVERT(time, '11:59:59')
	AND (Company in ('11','12','14')) 
	AND vwPlogdataFix.Event=28 --32=проход зафиксирован 
	--AND vwPlogdataFix.MODE=1 --нужно оба состояния 							    
) As t12AM


IF OBJECT_ID(N'tempdb..#plogdata1PM', 'U') IS NOT NULL
DROP TABLE #plogdata1PM
SELECT * INTO #plogdata1PM FROM
( 
	SELECT 
		pList.Name + ' ' + SUBSTRING(pList.FirstName,1,1) + '. '+ SUBSTRING( pList.MidName,1,1) + '.' as FIO, --[Фамилия И. О.]
		pList.TabNumber,
		PCompany.Name as Company, 
		PDivision.Name as Otdel,
		PPost.Name as Doljnost,
		vwPlogdataFix.TimeVal,
		vwPlogdataFix.Remark,
		vwPlogdataFix.Mode,
		vwplogdataFix.HozOrgan	
	FROM           PList
		 left join PDivision     on Plist.Section = PDivision.ID 
		 left join PPost         on Plist.Post    = PPost.ID 
		 left join PCompany      on PList.Company = PCompany.ID
		 left join vwPlogdataFix on PList.ID      = vwPlogdataFix.HozOrgan
    WHERE  vwPlogdataFix.TimeVal BETWEEN @Datetime1 AND @Datetime2
	AND  DATEPART(WEEKDAY,vwPlogdataFix.TimeVal) not in (6,7)
    AND  CONVERT(time, vwPlogdataFix.TimeVal) BETWEEN CONVERT(time, '12:48:00') AND CONVERT(time, '13:10:00')
	AND (Company in ('11','12','14')) 
	AND vwPlogdataFix.Event=28 --32=проход зафиксирован 
							    
) As t1PM

SET DateFirst 1
IF OBJECT_ID(N'tempdb..#plogdata5PM', 'U') IS NOT NULL
DROP TABLE #plogdata5PM
SELECT * INTO #plogdata5PM FROM
( 
	SELECT 
		pList.Name + ' ' + SUBSTRING(pList.FirstName,1,1) + '. '+ SUBSTRING( pList.MidName,1,1) + '.' as FIO, --[Фамилия И. О.]
		pList.TabNumber,
		PCompany.Name as Company, 
		PDivision.Name as Otdel,
		PPost.Name as Doljnost,
		vwPlogdataFix.TimeVal,
		vwPlogdataFix.Remark,
		vwPlogdataFix.Mode,
		vwplogdataFix.HozOrgan	
	FROM           PList
		 left join PDivision     on Plist.Section = PDivision.ID 
		 left join PPost         on Plist.Post    = PPost.ID 
		 left join PCompany      on PList.Company = PCompany.ID
		 left join vwPlogdataFix on PList.ID      = vwPlogdataFix.HozOrgan
    WHERE  vwPlogdataFix.TimeVal BETWEEN @Datetime1 AND @Datetime2
	AND  DATEPART(WEEKDAY,vwPlogdataFix.TimeVal) not in (6,7)
    AND  (CASE WHEN DATEPART(WEEKDAY,vwPlogdataFix.TimeVal) in  (1,2,3,4)
	          THEN ( CASE WHEN CONVERT(time, vwPlogdataFix.TimeVal) BETWEEN CONVERT(time, '16:45:00') AND CONVERT(time, '23:59:59') THEN 'nnn' END)
          end) IS NOT NULL 
	AND (Company in ('11','12','14')) --Управление ЦЭС ЮЭС
	AND vwPlogdataFix.Event=28 --32=проход зафиксирован 
	AND vwPlogdataFix.MODE=2  -- для раннего ухода нас интересуют только выходы							    
) As t5PM


SET DateFirst 1
IF OBJECT_ID(N'tempdb..#plogdata5PMFriday', 'U') IS NOT NULL
DROP TABLE #plogdata5PMFriday
SELECT * INTO #plogdata5PMFriday FROM
( 
	SELECT 
		pList.Name + ' ' + SUBSTRING(pList.FirstName,1,1) + '. '+ SUBSTRING( pList.MidName,1,1) + '.' as FIO, --[Фамилия И. О.]
		pList.TabNumber,
		PCompany.Name as Company, 
		PDivision.Name as Otdel,
		PPost.Name as Doljnost,
		vwPlogdataFix.TimeVal,
		vwPlogdataFix.Remark,
		vwPlogdataFix.Mode,
		vwplogdataFix.HozOrgan	
	FROM           PList
		 left join PDivision     on Plist.Section = PDivision.ID 
		 left join PPost         on Plist.Post    = PPost.ID 
		 left join PCompany      on PList.Company = PCompany.ID
		 left join vwPlogdataFix on PList.ID      = vwPlogdataFix.HozOrgan
    WHERE  vwPlogdataFix.TimeVal BETWEEN @Datetime1 AND @Datetime2
	AND  DATEPART(WEEKDAY,vwPlogdataFix.TimeVal) not in (6,7)
    AND  (CASE WHEN DATEPART(WEEKDAY,vwPlogdataFix.TimeVal) = 5 
	          THEN ( CASE WHEN CONVERT(time, vwPlogdataFix.TimeVal) BETWEEN CONVERT(time, '15:45:00') AND CONVERT(time, '23:59:59') THEN 'nnn' END)
          end) IS NOT NULL 
	AND (Company in ('11','12','14'))
	AND vwPlogdataFix.Event=28 --32=проход зафиксирован 
	AND vwPlogdataFix.MODE=2  -- для раннего ухода нас интересуют только выходы							    
) As t5PMFriday
------------------------------------------------------------------------------------------------------------------------------------------------------------
IF OBJECT_ID(N'tempdb..##plogdataALLeTime', 'U') IS NOT NULL
DROP TABLE ##plogdataALLeTime
SELECT * INTO ##plogdataALLeTime
FROM (
---------------------------НАЧАЛО---8:00-8:30---------------------------------------------------------------------------------------------------------------

SELECT  FIO,
		TabNumber,
		Company, 
		Otdel,
		Doljnost,
		(CONVERT(date, TimeVal)) as [Date],
		(CONVERT(varchar(10), TimeVal,8)) as [Time],
		Remark,
		case Mode when 1 then 'ВХОД' else convert(varchar,mode) end as mode,
		'Опоздание на работу' as eTime
FROM #plogdata8AM
INNER JOIN (select min(TimeVal) as minTimeVal,HozOrgan from #plogdata8AM
            where CONVERT(time, #plogdata8AM.TimeVal) BETWEEN CONVERT(time, '8:00:00') AND CONVERT(time, '8:30:00') 
			group by convert(date,TimeVal),HozOrgan 
			) as minOpozdInDay  --фильтрация повторных входов с 8 до 8 30
			  on #plogdata8AM.HozOrgan = minOpozdInDay.HozOrgan
			 and #plogdata8AM.TimeVal  = minOpozdInDay.minTimeVal   
WHERE CONVERT(time, #plogdata8AM.TimeVal) BETWEEN CONVERT(time, '8:00:00') AND CONVERT(time, '8:30:00')  --искомые записи с 8 до 8 30	
      AND not EXISTS (select #plogdata8AM.TimeVal from #plogdata8AM as plgd   -- И не существует входа до 8 00
                      where  convert (time,timeval) BETWEEN CONVERT(time, '5:00:00') AND CONVERT(time, '7:59:59') 
                      and #plogdata8AM.HozOrgan=plgd.HozOrgan and convert(date,#plogdata8AM.TimeVal)=convert(date,plgd.TimeVal) )               				 

---------------------------КОНЕЦ---8:00-8:30------------------------------------------------------------------------------------------------------------------
UNION ALL
---------------------------НАЧАЛО---11:15-11:59:59------------------------------------------------------------------------------------------------------------
--select * from #plogdata12AM
SELECT-- OutDoor.HozOrgan,
	   OutDoor.FIO,
	   OutDoor.TabNumber,
	   OutDoor.Company,
	   OutDoor.Otdel,
	   OutDoor.Doljnost,
	   (CONVERT(date, OutDoor.MaxOutDoor)) as [Date],
	   (CONVERT(varchar(10), OutDoor.MaxOutDoor,8)) as [Time],
	   OutDoor.Remark,
	   case OutDoor.Mode when 2 then 'ВЫХОД' else convert(varchar,OutDoor.Mode) end as mode,
	   'Ранний уход на обед' as eTime		 
FROM (
      select FIO,TabNumber,Company,Otdel,Doljnost,Remark,Mode,
		     max(TimeVal) as MaxOutDoor,HozOrgan --макс выход
      from #plogdata12AM
      where mode=2 --2=выход
      group  by HozOrgan,convert(date,Timeval)
	            ,FIO,TabNumber,Company,Otdel,Doljnost,Remark,Mode 
	 ) as OutDoor
left join
     (
	  select 
	        FIO,TabNumber,Company,Otdel,Doljnost,Remark,Mode, 
	        max(TimeVal) as MaxEnter,HozOrgan  --макс вход
      from #plogdata12AM
      where mode=1  --1=вход
      group  by HozOrgan,convert(date,Timeval)
	           ,FIO,TabNumber,Company,Otdel,Doljnost,Remark,Mode 
     ) as Enter
ON OutDoor.HozOrgan = Enter.HozOrgan   and convert(date,OutDoor.MaxOutDoor)=convert(date,Enter.MaxEnter)
WHERE case when MaxEnter is null then 'Ранний уход на обед'
	       when MaxOutDoor>MaxEnter then 'Ранний уход на обед'
	  else 'ELSE'
	  end = 'Ранний уход на обед'

---------------------------КОНЕЦ---11:15-11:59:59------------------------------------------------------------------------------------------------------------
UNION ALL
---------------------------НАЧАЛО---12:48-13:10--------------------------------------------------------------------------------------------------------------
--select * from #plogdata1PM
SELECT
	   Enter.FIO,
	   Enter.TabNumber,
	   Enter.Company,
	   Enter.Otdel,
	   Enter.Doljnost,
	   (CONVERT(date, Enter.MinEnter)) as [Date],
	   (CONVERT(varchar(10), Enter.MinEnter,8)) as [Time],
	   --outdoor.MODE,OutDoor.MinOutDoor,	 
	   Enter.Remark,
	   case Enter.Mode when 1 then 'ВХОД' else convert(varchar,Enter.Mode) end as mode,
	   'Позднее возвращение с обеда' as eTime
	   --case when MinOutDoor is null then 'Позднее возвращение с обеда'
	   --     when MinEnter < MinOutDoor then 'Позднее возвращение с обеда'
	   --else 'ELSE' end as eTime
FROM (
      select FIO,TabNumber,Company,Otdel,Doljnost,Remark,Mode,
		     min(TimeVal) as MinEnter,HozOrgan --мin вход
      from #plogdata1PM
      where mode=1 --1=вход
      group  by HozOrgan,convert(date,Timeval)
	            ,FIO,TabNumber,Company,Otdel,Doljnost,Remark,Mode 
	 ) as Enter
left join
     (
	  select 
	        FIO,TabNumber,Company,Otdel,Doljnost,Remark,Mode, 
	        min(TimeVal) as MinOutDoor,HozOrgan  --макс вход
      from #plogdata1PM
      where mode=2  --2=выход
      group  by HozOrgan,convert(date,Timeval)
	           ,FIO,TabNumber,Company,Otdel,Doljnost,Remark,Mode 
     ) as OutDoor
ON Enter.HozOrgan = Outdoor.HozOrgan   and convert(date,Enter.MinEnter)=convert(date,OutDoor.MinOutDoor)
WHERE case when MinOutDoor is null then 'Позднее возвращение с обеда'
	       when MinEnter<MinOutDoor then 'Позднее возвращение с обеда'
	  else 'ELSE'
	  end = 'Позднее возвращение с обеда'
---------------------------Конец---12:48-13:10--------------------------------------------------------------------------------------------------------------	
UNION ALL
---------------------------Начало---16:45:00-16:59:59-------------------------------------------------------------------------------------------------------
select FIO,
	   TabNumber,
	   Company,
	   Otdel,
	   Doljnost,
	   (CONVERT(date, TimeVal)) as [Date],
	   (CONVERT(varchar(10),TimeVal,8)) as [Time],
	   --Timeval,
	   Remark,
	   case Mode when 2 then 'ВЫХОД' else convert(varchar,Mode) end as mode,
	   'Ранний уход с работы' as eTime
from  #plogdata5PM

INNER JOIN (select max(TimeVal) as maxTimeVal,HozOrgan from #plogdata5PM  --фильтрация повторных выходов с 16:45 до 16:59:59, берем только  последний выход.
            where CONVERT(time, #plogdata5PM.TimeVal) BETWEEN CONVERT(time, '16:45:00') AND CONVERT(time, '16:59:59') 
			group by convert(date,TimeVal),HozOrgan 
			) as MaxRanniyOutInDay 
on  #plogdata5PM.HozOrgan  =  MaxRanniyOutInDay.HozOrgan
and #plogdata5PM.TimeVal   =  MaxRanniyOutInDay.maxTimeVal   

WHERE
     CASE WHEN CONVERT(time, TimeVal) BETWEEN CONVERT(time, '16:45:00') AND CONVERT(time, '16:59:59') THEN 'Ранний уход с работы' END = 'Ранний уход с работы' 
 AND not EXISTS (select #plogdata5PM.TimeVal from #plogdata5PM as plgd   -- И не существует выхода после 17 00
                      where  convert (time,timeval) BETWEEN CONVERT(time, '17:00:00') AND CONVERT(time, '23:59:59') 
                      and #plogdata5pm.HozOrgan=plgd.HozOrgan and convert(date,#plogdata5PM.TimeVal)=convert(date,plgd.TimeVal) ) 
--order by fio,timeval
---------------------------Конец---16:45:00-16:59:59-------------------------------------------------------------------------------------------------------
UNION ALL
---------------------------Начало---15:45:00-15:59:59------------------------------------------------------------------------------------------------------
select FIO,
	   TabNumber,
	   Company,
	   Otdel,
	   Doljnost,
	   (CONVERT(date, TimeVal)) as [Date],
	   (CONVERT(varchar(10),TimeVal,8)) as [Time],
	   --Timeval,
	   Remark,
	   case Mode when 2 then 'ВЫХОД' else convert(varchar,Mode) end as mode,
	   'Ранний уход с работы' as eTime
from  #plogdata5PMFriday

INNER JOIN (select max(TimeVal) as maxTimeVal,HozOrgan from #plogdata5PMFriday  --фильтрация повторных выходов с 15:45 до 15:59:59, берем только  последний выход.
            where CONVERT(time, #plogdata5PMFriday.TimeVal) BETWEEN CONVERT(time, '15:45:00') AND CONVERT(time, '15:59:59') 
			group by convert(date,TimeVal),HozOrgan 
			) as MaxRanniyOutInDay 
on  #plogdata5PMFriday.HozOrgan  =  MaxRanniyOutInDay.HozOrgan
and #plogdata5PMFriday.TimeVal   =  MaxRanniyOutInDay.maxTimeVal   

WHERE
     CASE WHEN CONVERT(time, TimeVal) BETWEEN CONVERT(time, '15:45:00') AND CONVERT(time, '15:59:59') THEN 'Ранний уход с работы' END = 'Ранний уход с работы' 
 AND NOT EXISTS (select #plogdata5PMFriday.TimeVal from #plogdata5PMFriday as plgd   -- И не существует выхода после 16 00
                 where  convert (time,timeval) BETWEEN CONVERT(time, '16:00:00') AND CONVERT(time, '23:59:59') 
                      and #plogdata5PMFriday.HozOrgan=plgd.HozOrgan and convert(date,#plogdata5PMFriday.TimeVal)=convert(date,plgd.TimeVal) ) 
---------------------------Конец---15:45:00-15:59:59----------------------------------------------------------------------------------------------------------
) AS ALLeTime
--ORDER BY FIO, eTime
Drop table #plogdata8AM 
Drop table #plogdata12AM
Drop table #plogdata1PM
Drop table #plogdata5PM
Drop table #plogdata5PMFriday

Select * from ##plogdataALLeTime
ORDER BY company,otdel,FIO,[date],[time] 
--------------------------------------------------------------------------------------------------------------------------------------------------------------

END

