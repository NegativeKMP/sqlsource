SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		KalinichenkoMP
-- Create date: 20170718
-- =============================================
ALTER PROCEDURE [fls].[spApproveLink] 
	(@GUID uniqueidentifier,
     @TimeEndDwnld datetime)	
AS
BEGIN
	SET NOCOUNT ON;    
		BEGIN TRY
		  Begin Transaction trApproveLink
		      SELECT [GUID] FROM fls.tbFileLink WHERE GUID = @GUID
			  if @@ROWCOUNT > 0 
		       UPDATE [fls].[tbFileLink]
			   SET TimeEndDwnld = @TimeEndDwnld
			   WHERE GUID=@GUID
			  else  RAISERROR (15600,-1,-1, 'RaisError With not found GUID'); 
		  Commit Transaction trApproveLink;
			RETURN (1);
         END TRY
		 BEGIN CATCH --обработка ошибки
		    begin
			    SELECT ERROR_NUMBER() AS ErrorNumber,
			           ERROR_SEVERITY() AS ErrorSeverity,
			           ERROR_STATE() AS ErrorState,
			           ERROR_PROCEDURE() AS ErrorProcedure,
			           ERROR_LINE() AS ErrorLine,
			           ERROR_MESSAGE() AS ErrorMessage;
		        Rollback Transaction trApproveLink;  --откат тразнакции
				RETURN(0);
  			end;
		 END CATCH;
END 
