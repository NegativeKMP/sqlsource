SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--============================================================
--KalinichenkoMP 06.05.2016 Вывод итогов по нарушителям режима
--============================================================
ALTER procedure [dbo].[spResumeOpozd]
AS 
BEGIN
set nocount on
set language us_english
SET DATEFIRST 1 

	IF OBJECT_ID ('tempdb..##plogdataALLeTime') IS NOT NULL 
	BEGIN
			select ISNULL(company,'Unknown PCompany.Name') as company,ISNULL(otdel,'Unknown PDivision.Name') as otdel,fio,
			 [Опоздание на работу],[Ранний уход на обед],[Позднее возвращение с обеда],[Ранний уход с работы]
			,[Опоздание на работу]+
			 [Ранний уход на обед]+
			 [Позднее возвращение с обеда]+
			 [Ранний уход с работы] as Summ
			FROM
				(select company,otdel,fio,etime from ##plogdataALLeTime) as Sourcetb
				PIVOT
				(
				count(etime)
				  FOR etime IN ([Опоздание на работу],[Ранний уход на обед],[Позднее возвращение с обеда],[Ранний уход с работы])
				) AS  Pivottb
			--where  fio like '%%'
			order by Pivottb.company,pivottb.Otdel,Pivottb.FIO

			IF OBJECT_ID ('tempdb..##plogdataALLeTime') IS NOT NULL
			drop table ##plogdataALLeTime
			
	END
	ELSE  SELECT '' as company,'' as otdel,'' as fio,'' as [Опоздание на работу],'' as [Ранний уход на обед],'' as [Позднее возвращение с обеда],'' as [Ранний уход с работы],'' as  Summ

END
