/****** Object:  View [dbo].[vwDeliveryPoints_Export]    Script Date: 09.08.2019 16:03:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



ALTER view [dbo].[vwDeliveryPoints_Export] 
as (

/*
select * from [dbo].[vwAll_Schet_CK11]
select * from [dbo].[vwPotrebiteli_CK11]
select * from [dbo].[vwObject_CK11]
select * from [dbo].[vwCodings_ck11]
*/

select 
   --Potr.[Name] + N' '+ Potr.OficialName + N' '+ Potr.Patronymic as A --������������(Customer.name)
   Case when vwCods.KeyRegion = 999 then  potr.OficialName
        else  Potr.[Name] + N' '+ Potr.OficialName + N' '+ Potr.Patronymic
   end  as A --������������(Customer.name)
  ,Potr.mRID as B                  --���������� ������������� (Customer.mRID)
  ,vwObject_CK11.NameObject as C  --������������(ServiceLocation.name)
  ,vwObject_CK11.mRID as D       --���������� �������������(ServiceLocation.mRID)
  ,potr.FactAddr as E          --����� (ServiceLocation.mainAddress)
  ,vwObject_CK11.Category as F --��������� ����������(ServiceLocation.supplyReliabilityRank)
  , case when N'7'+Right(ltrim(rtrim(Mobilephone)),10) =N'70000000000' then N''
    else N'7'+Right(ltrim(rtrim(Mobilephone)),10) 
	end  as G --�������(ServiceLocation.phone1)
  ,N'' as H  --����� �������
  ,N'' as I  --������������� ��������� TODO
  ,N'' as J  --����� ������(ServiceLocation.numOfStoreys)
  ,N'' as K  --�������� ���. �� (ServiceLocation.standbyCapacity)
  ,N'' as L  --"���������� (ServiceLocation.significance)"
  ,N'' as M  --������ ������������ (ServiceLocation.ServiceContingencyCategory)
  ,N'' as N  --�.�.� ������������� (ServiceLocation.inChargeFullName)
  ,N'��' as O --"����� ��������?(UsagePoint.isSdp)"
  ,N'' as P --��������� �����(UsagePoint.emergencyPowerReserve)
  ,N'' as Q --��������������� ����� (UsagePoint.technoPowerReserve)
  ,N'' as R --����������������� ��������������� ����� (UsagePoint.technoPowerReserveDuration)
  --,vwCods.NTP  as S  --���������� ������������ (Substation)
  ,case when vwCods.keyBalans = N'1' then vwCods.NTP+N'�'  -- ����������� �����������
	    else vwCods.NTP
   end as S  --���������� ������������ (Substation)
  ,vwCods.NTTTP  as T --�������� ������������� 
  ,vwCods.NLineTP as U --�������������
  --,vwCods.Napr  as V  --������� ����������
  ,case when vwCods.Napr = N'006' then N'6 ��'
		when vwCods.Napr = N'010' then N'10 ��'
		when vwCods.Napr = N'110' then N'110 ��'
		when vwCods.Napr = N'002' then N'0.4 ��'
		when vwCods.Napr = N'004' then N'0.4 ��'
		else vwCods.Napr end as V --������� ����������

  --,N'0,4 ��' as  V  --������� ����������
  ,vwCods.NSekSHin  as W --�������� ������� ����� ������������ ����� ��������
  ,vwCods.mRID as X -- ���������� ������������� (UsagePoint.mRID)
  ,Case when NUchet = N'1' then N'��'
        else N'���'
   end	as Y   -- ����������� ��� ������������?(Meter.isCommercial)
  ,N'���' as Z   --�����������?(Meter.isVirtual)
  ,N'0' as AA  --"��� ��(Meter.meteringPointCode)"
  ,vwALL.NSchetIndustr as AB  --�������� �����(Meter.serialNumber)
  ,N'' as AC  --������� ����� � ������� �����
  ,vwALL.mRID AS AD  --���������� ������������� (Meter.mRID) 
FROM [dbo].[vwCodings_ck11] vwCods
LEFT JOIN  dbo.vwPotrebiteli_CK11 Potr                                                               
   on Potr.NLSchet = vwCods.NLSchet AND                                                           
      Potr.KeyOtdSbyt = vwCods.KeyOtdSbyt AND                                                     
      Potr.KeyRegion = vwCods.KeyRegion AND                                                       
      Potr.KeySbyt = vwCods.KeySbyt   AND                                                          
      Potr.KeyContract = vwCods.KeyContract      
LEFT JOIN [dbo].[vwAll_Schet_CK11] vwALL                                                            
   on vwCods.NTochUchet = vwALL.NSchet 	                                                
LEFT JOIN vwObject_CK11 
   on vwObject_CK11.[KeyContract]  =   vwCods.KeyContract and
	  vwObject_CK11.[KeyOtdSbyt]	=  vwCods.KeyOtdSbyt AND 
	  vwObject_CK11.[KeyObject]	   =   vwCods.KeyObject and
	  vwObject_CK11.[NLSchet]		=  vwCods.NLSchet and 
	  vwObject_CK11.[KeyRegion]	   =   vwCods.KeyRegion and 
	  vwObject_CK11.[KeySbyt]		=  vwCods.KeySbyt
WHERE  vwCods.NRes = N'10'
   and vwCods.Napr  not in (N'002',N'004')
    
)





GO


