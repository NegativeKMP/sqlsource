/****** Object:  StoredProcedure [web].[spUserWeb_Add]    Script Date: 09.08.2019 16:29:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Калиниченко МП
-- Create date: 12.05.2017
-- Description:	добюавление  пользователя  
-- Input login, password - varchar(50)
-- Output success bit
-- =============================================
ALTER PROCEDURE [web].[spUserWeb_Add]
(
   @Login varchar(50),
   @Pass  varchar(50),
   @Id_Role int,
   @Success bit = 0 OUTPUT,
   @NewID int = -1 OUTPUT,
   @Description varchar(100) = '' output 
)
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT TOP (1) id FROM web.tbUsrWeb WHERE Login=@Login
	IF @@ROWCOUNT=0
	  BEGIN
		 SELECT TOP(1) id from web.tbRoleRightWeb where id=@Id_Role
		 IF @@ROWCOUNT > 0
		    Begin
			  INSERT INTO web.tbUsrWeb ([Login],[Password],[idRightWeb_FK]) VALUES (@Login,@Pass,@Id_Role)
			  IF @@ROWCOUNT>0 
				begin 
				     SET @Success=1
					 SET @Description = 'Пользователь '+@Login+' добавлен' 
					 SET @NewID = (SELECT top(1) id FROM web.tbUsrWeb WHERE Login=@Login)
					 PRINT 'Пользователь '+@Login+' добавлен' 
                end
			End
			Else 
			  begin 
			      set @Success=0  
				  SET @Description = 'Нет идентификатора роли в таблице' 
				  PRINT 'Нет идентификатора роли в таблице' 
			  end
      END
	  ELSE 
		begin 
		   set @Success=0  -- такой логин существует
		   SET @Description = 'Пользователь '+@Login+' уже существует'
		   PRINT 'Пользователь '+@Login+' уже существует'
        end
END
